# FIREWALLS


## What is a Firewall?
A firewall is a cybersecurity tool that can either be a software programme or hardware device that helps in averting intruders from getting access to unauthorised networks by monitoring incoming and outgoing network traffic, thereby deciding whether to allow or block specific traffic based on a defined set of security rules.


It mimics a gatekeeper for private networks by not allowing data packets through them unless they are coming from a trusted source.


It can also be used for content filtering. For example, when a college doesn't want its students to access Google during exams, it can configure the college firewall in such a way that students can't access Google.com. Present-day computers have inbuilt software firewalls.


## How does a firewall work?


We know that the internet is an untrusted network of connected computers. Whenever we search for anything in the web browser, we may get output from external networks where intruders or hackers may exist. They may try to snoop into our device by sending a virus in reply to our request.


This is where a firewall comes into the picture; it maintains a list of trusted sources and trusted destinations, and it not only blocks all the data from untrusted networks but also blocks the device in a network from accessing untrusted destinations.


Firewalls examine the incoming and outgoing traffic at the computer's entry points, called ports, thereby blocking the data from untrusted devices. They use packet switching, stateful inspection, and proxy techniques to filter the data.


## Types of Firewalls


- ### Based on the Structure :
    There are three types of firewalls based on their structure. They are hardware firewalls, software firewalls, and hybrid firewalls.


1. Hardware Firewalls: They are otherwise called "appliance firewalls." It is a physical device that will be placed between an external network and a gateway.


2. Software firewalls: This is another name for host firewalls. It is a simple programme installed on a computer that works through port numbers and other installed software.


3. Hybrid Firewalls: A combination of both hardware and software firewalls


- ### Based on their features and the level of security they provide

1. Packet-filtering Firewalls: They are the oldest, cheapest, and fastest basic firewalls that operate at the network layer. They monitor network traffic and filter incoming packets based on configured security rules. is a data packet that does not match the rules just blocked?



2. Circuit-level gateway firewall: This is another type of firewall that can control the traffic and also simultaneously process the transactions. It works at the session layer of the OSI model. It ensures the safety of the connections that are established.


3. Application-level gateways, also called proxy firewalls, work at the application layer of the OSI model. The functionality of proxy firewalls is to act as an intermediary between the client and the server. Here, proxy firewalls act as clients and accept only legitimate data from the web server, ensuring that the client is protected from suspicious servers.


4. Cloud firewalls: These are also called Faas (firewall as a service). The purpose of the cloud firewall is to prevent malicious, suspicious, or malicious networks from the users or clients. The main difference between cloud firewalls and other firewalls is that cloud firewalls operate in the cloud. They act as barriers for the client's protection from the attackers.


## Disadvantages of Firewalls
The cost of a firewall varies with the level of sophistication. This cost not only involves the cost of the firewall but also the cost of the people who manage these firewalls, as managing them requires expertise. So it needs professionals to manage.
From the moment we turn it on, our firewalls start running in the background. Every time we open a new tab, the service has to satisfy the criteria of the firewalls, which eventually leads to a decrease in the performance of the computer.


Firewalls are designed in such a way as to prevent unauthorised network intrusions, but they do not care about what legitimate users are sending, that is, the contents of a file sent by legitimate users. If an intruder sends an email that has malware, a firewall can't prevent it, which in turn leads to the compromise of a system.



A firewall is designed to block activities that look suspicious. Unfortunately, a block can also extend to legitimate network-intensive processes. In some cases, even running a legitimate program, such as a messaging programme or social networking plug-in, won't work if your firewall concludes it's a malicious process.


## References


1. https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6

2. https://www.checkpoint.com/cyber-hubwork-security/what-is-firewall
1. https://www.forcepoint.com/cyber-edu/firewall
1. https://www.javatpoint.com/firewall
1. https://www.simplilearn.com/tutorials/cyber-security-tutorial/what-is-firewall
1. https://courses.cs.washington.edu/courses/csep561/97sp/paper1/paper09/
1. https://www.geeksforgeeks.org/introduction-of-firewall-in-computer-network/
1. https://www.cisco.com/c/en_in/products/security/firewalls/what-is-a-firewall.html
1. https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls
1. https://www.compuquip.com/blog/types-firewall-architectures