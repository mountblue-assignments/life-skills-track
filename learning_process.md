# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique? Paraphrase the video in your own words

If we want to understand anything better we actually have to explain what we understood to others. We can understand the things better when we explain what we understood to others and it also helps to assess what we understood about a particular topic. This technique is called the Feynman technique. It is named after a scientist called Richard Feynman as he was a great teacher and used to explain complex problems in the simplest language which would be easily understood by laymen. One of his principles was that "You must not fool yourself and you are the easiest persons to fool".



### What are the different ways to implement this technique in your learning process?

There are two ways in which I am going to implement this technique. In the first approach, I start by writing the concept which I learned on a page. Further, I write what I understood about that topic in simple words as if I am teaching someone else. If I feel that I am not clear on a particular portion of the topic, I will refer to the sources again. Finally, I will find out what are the difficult terms in that topic and explore better ways to simplify those terms.
In my second approach, firstly I will choose a topic. Later I will try to explain that to a child and identify gaps in my explanation. Go back to the source material to better understand it. I will try to simplify my answer further. Finally, I organize it better and present it again.

##  2. Learning How to Learn

### Paraphrase the video in detail in your own words.

Generally, there are two modes a person can be in, if he sits to learn something there are focus mode and relax mode. In focus mode, one won't allow any thoughts between his work, it is the other way around with the relax mode where there will be a free flow of thoughts. So whenever we sit to learn something we have to switch between the modes for a free flow of ideas to retain and understand better.Whenever one feels tired from being in focus mode for a long time, it is better to shift to a diffuse mode for the free flow of ideas and to improve productivity.

The other way of learning is the Pomodoro technique. In this technique, we try to set time for being in focus mode and diffuse mode as well. There are some more ways by which we can improve our learning, like giving some mini tests after learning something, doing the problems multiple times to improve endurance, try to recall everything that we have read after reading a few pages. Understanding is important but when it is combined with practice and repetition to master any subject.


### What are some of the steps that you can take to improve your learning process?

Whenever I decide to learn something, I collect all the sources at once and I try to keep away all the things that distract me. I try to shift between focus mode and defuse mode to improve my thinking capacity. I try to revise whatever I studied at regular intervals. I practices some questions on the topic I was learning and practiced them multiple times to increase my understanding and speed to solve such questions.

## 3. How to learn anything in 20 hrs

### Paraphrase the video in detail in your own words.

we won't retain anything that we learned if we won't practice what we learn. we will get better and better with practice. One way to rapid skill acquisition is by following four steps. The first step is to divide what we want to learn into chunks. Secondly, learning enough about a particular topic to self-correct ourselves without any procrastination. Thirdly, remove barriers that are stopping us to learn and practice. lastly, practice what we learned for twenty hours.

### What are some of the steps that you can take to improve your learning process?

Whenever I sit to study, I make sure that there were no such things that distract me. I segregate what have to learn into time slots. practice as much time as I learned the topic. Practice what I learn enough times so that I can make it the quick time when asked to solve a question on that topic. Revise as many times as possible so that it stuck in my mind. I try to revise whatever I studied at regular intervals. I will give multiple tests on a topic to retain a better.
