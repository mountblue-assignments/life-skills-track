# Python Cheatsheet

## Keywords

    and	  -  A logical operator

    as	  -  To create an alias

    assert  - 	For debugging

    break  - 	To break out of a loop

    class  - 	To define a class

    continue  - 	To continue to the next iteration of a loop

    def  - 	To define a function

    del  - 	To delete an object

    elif  - 	Used in conditional statements, same as else if

    else  - 	Used in conditional statements

    except  - 	Used with exceptions, what to do when an exception occurs

    False  - 	Boolean value, result of comparison operations

    finally  - 	Used with exceptions, a block of code that will be executed no matter if there is an exception or not

    for  - 	To create a for loop

    from  - 	To import specific parts of a module

    global  - 	To declare a global variable

    if  - 	To make a conditional statement

    import  - 	To import a module

    in  - 	To check if a value is present in a list, tuple, etc.

    is  - 	To test if two variables are equal

    lambda	  - To create an anonymous function

    None  - 	Represents a null value

    nonlocal  - 	To declare a non  - local variable

    not  - 	A logical operator

    or  - 	A logical operator

    pass  - 	A null statement, a statement that will do nothing

    raise  - 	To raise an exception

    return  - 	To exit a function and return a value

    True  - 	Boolean value, result of comparison operations

    try  - 	To make a try...except statement

    while  - 	To create a while loop

    with  - 	Used to simplify exception handling

    yield  - 	To end a function, returns a generator

---

## Build in functions

    abs()- 	Returns the absolute value of a number

    all()- 	Returns True if all items in an iterable object are true

    any()- 	Returns True if any item in an iterable object is true

    ascii()- 	Returns a readable version of an object. Replaces none-ascii characters with escape 
    character

    bin()- 	Returns the binary version of a number

    bool()- 	Returns the boolean value of the specified object

    bytearray()- 	Returns an array of bytes

    bytes()- 	Returns a bytes object

    callable()- 	Returns True if the specified object is callable, otherwise False

    chr()- 	Returns a character from the specified Unicode code.

    classmethod()- 	Converts a method into a class method

    compile()- 	Returns the specified source as an object, ready to be executed

    complex()- 	Returns a complex number

    delattr()- 	Deletes the specified attribute (property or method) from the specified object

    dict()- 	Returns a dictionary (Array)

    dir()- 	Returns a list of the specified object's properties and methods

    divmod()- 	Returns the quotient and the remainder when argument1 is divided by argument2

    enumerate()- 	Takes a collection (e.g. a tuple) and returns it as an enumerate object

    eval()- 	Evaluates and executes an expression

    exec()- 	Executes the specified code (or object)

    filter()- 	Use a filter function to exclude items in an iterable object

    float()- 	Returns a floating point number

    format()- 	Formats a specified value

    frozenset()- 	Returns a frozenset object

    getattr()- 	Returns the value of the specified attribute (property or method)

    globals()- 	Returns the current global symbol table as a dictionary

    hasattr()- 	Returns True if the specified object has the specified attribute (property/method)

    hash()- 	Returns the hash value of a specified object

    help()- 	Executes the built-in help system

    hex()- 	Converts a number into a hexadecimal value

    id()- 	Returns the id of an object

    input()- 	Allowing user input

    int()- 	Returns an integer number

    isinstance()- 	Returns True if a specified object is an instance of a specified object

    issubclass()- 	Returns True if a specified class is a subclass of a specified object

    iter()- 	Returns an iterator object

    len()- 	Returns the length of an object

    list()- 	Returns a list

    locals()- 	Returns an updated dictionary of the current local symbol table

    map()- 	Returns the specified iterator with the specified function applied to each item

    max()- 	Returns the largest item in an iterable

    memoryview()- 	Returns a memory view object

    min()- 	Returns the smallest item in an iterable

    next()- 	Returns the next item in an iterable

    object()- 	Returns a new object

    oct()- 	Converts a number into an octal

    open()- 	Opens a file and returns a file object

    ord()- 	Convert an integer representing the Unicode of the specified character

    pow()- 	Returns the value of x to the power of y

    print()- 	Prints to the standard output device

    property()- 	Gets, sets, deletes a property

    range()- 	Returns a sequence of numbers, starting from 0 and increments by 1 (by default)

    repr()- 	Returns a readable version of an object

    reversed()- 	Returns a reversed iterator

    round()- 	Rounds a numbers

    set()- 	Returns a new set object

    setattr()- 	Sets an attribute (property/method) of an object

    slice()- 	Returns a slice object

    sorted()- 	Returns a sorted list

    staticmethod()- 	Converts a method into a static method

    str()- 	Returns a string object

    sum()- 	Sums the items of an iterator

    super()- 	Returns an object that represents the parent class

    tuple()- 	Returns a tuple

    type()- 	Returns the type of an object

    vars()- 	Returns the __dict__ property of an object

    zip()- 	Returns an iterator, from two or more iterators

    ---
    ## List Methods 
    append()-   -   	Adds an element at the end of the list

    clear()-   -   	Removes all the elements from the list

    copy()-   -   	Returns a copy of the list

    count()-   -   	Returns the number of elements with the specified value

    extend()-   -   	Add the elements of a list (or any iterable), to the end of the current list

    index()-   -   	Returns the index of the first element with the specified value

    insert()-   -   	Adds an element at the specified position

    pop()-   -   	Removes the element at the specified position

    remove()-   -   	Removes the first item with the specified value

    reverse()-   -   	Reverses the order of the list

    sort()-   -   	Sorts the list

    A list stores a series of items in a particular order. You access items using an index, or within a loop.

    Make a list

        bikes = ['trek', 'redline', 'giant']

    Get the first item in a list

        first_bike = bikes[0]

    Get the last item in a list

        last_bike = bikes[-1]

    Looping through a list

        for bike in bikes:
            print(bike)

    Adding items to a list

        bikes = []
        bikes.append('trek')
        bikes.append('redline')
        bikes.append('giant')

    Making numerical lists

        squares = []
        for x in range(1, 11):
        squares.append(x**2)

    List comprehensions

        squares = [x**2 for x in range(1, 11)]

    Slicing a list

        finishers = ['sam', 'bob', 'ada', 'bea']
        first_two = finishers[:2]

    Copying a list

        copy_of_bikes = bikes[:]


    Changing an element
        users = ['val', 'bob', 'mia', 'ron', 'ned']
        users[0] = 'valerie'
        users[-2] = 'ronald

    Deleting an element by its position

        del users[-1]

    Removing an item by its value

        users.remove('mia')

    Pop the last item from a list

    most_recent_user = users.pop()

    Pop the first item in a list

        first_user = users.pop(0)

    length of a list

        num_users = len(users)

    Sorting a list permanently

        users.sort()

    Sorting a list permanently in reverse alphabetical
    order

        users.sort(reverse=True)

    Sorting a list temporarily

        print(sorted(users))
        print(sorted(users, reverse=True))

    Reversing the order of a list
    users.reverse()

    Getting the first three items

        finishers = ['kai', 'abe', 'ada', 'gus', 'zoe']

        first_three = finishers[:3]

    Getting the middle three items

        middle_three = finishers[1:4]

    Getting the last three items

        last_three = finishers[-3:]

    Making a copy of a list

        finishers = ['kai', 'abe', 'ada', 'gus', 'zoe']

        copy_of_finishers = finishers[:]
--- 

## String Methods

    capitalize()-   	Converts the first character to upper case

    casefold()-   	Converts string into lower case

    center()-   	Returns a centered string

    count()-   	Returns the number of times a specified value occurs in a string

    encode()-   	Returns an encoded version of the string

    endswith()-   	Returns true if the string ends with the specified value

    expandtabs()-   	Sets the tab size of the string

    find()-   	Searches the string for a specified value and returns the position of where it was found

    format()-   	Formats specified values in a string

    format_map()-   	Formats specified values in a string

    index()-   	Searches the string for a specified value and returns the position of where it was found

    isalnum()-   	Returns True if all characters in the string are alphanumeric

    isalpha()-   	Returns True if all characters in the string are in the alphabet

    isascii()-   	Returns True if all characters in the string are ascii characters

    isdecimal()-   	Returns True if all characters in the string are decimals

    isdigit()-   	Returns True if all characters in the string are digits

    isidentifier()-   	Returns True if the string is an identifier

    islower()-   	Returns True if all characters in the string are lower case

    isnumeric()-   	Returns True if all characters in the string are numeric

    isprintable()-   	Returns True if all characters in the string are printable

    isspace()-   	Returns True if all characters in the string are whitespaces

    istitle()-    	Returns True if the string follows the rules of a title

    isupper()-   	Returns True if all characters in the string are upper case

    join()-   	Converts the elements of an iterable into a string

    ljust()-   	Returns a left justified version of the string

    lower()-   	Converts a string into lower case

    lstrip()-   	Returns a left trim version of the string
    maketrans()-   	Returns a translation table to be used in translations

    partition()-   	Returns a tuple where the string is parted into three parts

    replace()-   	Returns a string where a specified value is replaced with a specified value

    rfind()-   	Searches the string for a specified value and returns the last position of where it was found

    rindex()-   	Searches the string for a specified value and returns the last position of where it was found

    rjust()-   	Returns a right justified version of the string

    rpartition()-   	Returns a tuple where the string is parted into three parts

    rsplit()-   	Splits the string at the specified separator, and returns a list

    rstrip()-   	Returns a right trim version of the string

    split()-   	Splits the string at the specified separator, and returns a list

    splitlines()-   	Splits the string at line breaks and returns a list

    startswith()-   	Returns true if the string starts with the specified value

    strip()-   	Returns a trimmed version of the string

    swapcase()-   	Swaps cases, lower case becomes upper case and vice versa

    title()-   	Converts the first character of each word to upper case

    translate()-   	Returns a translated string

    upper()-   	Converts a string into upper case

    zfill()-   	Fills the string with a specified number of 0 values at the beginning

Indexing and Slicing Strings

    str = 'yellow'

    str[1]
    str[-1]
    str[4:6]
    str[:4]
    str[-3:]

Iterate String

    str = "hello"
    for c in str:
    print(c)

Built-in Function len()

    length = len("Hello")
    print(length)

String Concatination

    x = 'One fish, '
    y = 'two fish.'

    z = x + y

String Formating

    msg1 = 'Fred scored {} out of {} points.'

    msg1.format(3, 10)

    'Fred scored 3 out of 10 points.'

    msg2 = 'Fred {verb} a {adjective} {noun}.'

    msg2.format(adjective='fluffy', verb='tickled',
    noun='hamster')

    'Fred tickled a fluffy hamster.'

    greeting = "Welcome To Chili's"

    print(greeting.lower())

    text1 = '   apples and oranges   '

    text1.strip()       # => 'apples and oranges'

    text2 = '...+...lemons and limes...-...'

    # Here we strip just the "." characters
    
    text2.strip('.')    # => '+...lemons and limes...-'

    # Here we strip both "." and "+" characters

    text2.strip('.+')   # => 'lemons and limes...-'

    # Here we strip ".", "+", and "-" characters

    text2.strip('.+-')  # => 'lemons and limes'

    my_var = "dark knight"

    print(my_var.title()) 

    text = "Silicon Valley"

    print(text.split())     

    # Prints: ['Silicon', 'Valley']

    print(text.split('i'))  

    # Prints: ['S', 'l', 'con Valley']

    mountain_name = "Mount Kilimanjaro"

    print(mountain_name.find("o")) # Prints 1 in the console.

    fruit = "Strawberry"

    print(fruit.replace('r', 'R'))

    dinosaur = "T-Rex"

    print(dinosaur.upper()) 

    x = "-".join(["Codecademy", "is", "awesome"])

    print(x) 

--- 

## Dictionary Methods

    clear()-    	Removes all the elements from the dictionary

    copy()-    	Returns a copy of the dictionary

    fromkeys()-    	Returns a dictionary with the specified keys and value

    get()-    	Returns the value of the specified key

    items()-    	Returns a list containing a tuple for each key value pair

    keys()-    	Returns a list containing the dictionary's keys

    pop()-    	Removes the element with the specified key

    popitem()-    	Removes the last inserted key-value pair

    setdefault()-    	Returns the value of the specified key. If the key does not exist: insert the key, with the specified value

    update()-    	Updates the dictionary with the specified key-value pairs

    values()-    	Returns a list of all the values in the dictionary


Dictionaries

    alien_0 = {'color': 'green', 'points': 5}

    fav_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
    }

    # Show each person's favorite language.

    for name, language in fav_languages.items():
    
        print(name + ": " + language)

    for name in fav_languages.keys():

        print(name)

    for language in fav_languages.values():

        print(language)

    for name in sorted(fav_languages.keys()):

        print(name + ": " + language)

    alien_0 = {'color': 'green', 'points': 5}

    print(alien_0['color'])

    print(alien_0['points'])

    alien_0 = {'color': 'green'}

    alien_color = alien_0.get('color')

    alien_points = alien_0.get('points', 0)


    Modifying values in a dictionary

    alien_0 = {'color': 'green', 'points': 5}

    print(alien_0)

    # Change the alien's color and point value.

    alien_0['color'] = 'yellow'

    alien_0['points'] = 10

    print(alien_0)

    Deleting a key-value pair

    alien_0 = {'color': 'green', 'points': 5}

    print(alien_0)

    del alien_0['points']

    print(alien_0)

    Adding a key-value pair

    alien_0 = {'color': 'green', 'points': 5}

    alien_0['x'] = 0

    alien_0['y'] = 25
    
    alien_0['speed'] = 1.5
    
    Adding to an empty dictionary
    
    alien_0 = {}
    
    alien_0['color'] = 'green'
    
    alien_0['points'] = 5

    Finding a dictionary's length
    
    num_responses = len(fav_languages)

---
## Tuple Methods

    count()-  Returns the number of times a specified value occurs in a tuple

    index()-  Searches the tuple for a specified value and returns the position of where it was found

--- 
## File Methods 

    close()-    	Closes the file

    detach()-    	Returns the separated raw stream from the buffer

    fileno()-    	Returns a number that represents the stream, from the operating system's perspective

    flush()-    	Flushes the internal buffer

    isatty()-    	Returns whether the file stream is interactive or not

    read()-    	Returns the file content

    readable()-    	Returns whether the file stream can be read or not

    readline()-    	Returns one line from the file

    readlines()-    	Returns a list of lines from the file

    seek()-    	Change the file position

    seekable()-    	Returns whether the file allows us to change the file position

    tell()-    	Returns the current file position

    truncate()-    	Resizes the file to a specified size

    writable()-    	Returns whether the file can be written to or not

    write()-    	Writes the specified string to the file

    writelines()-    	Writes a list of strings to the file



---

